package com.buutcamp.countries;

public class Germany implements Country{
    private String officialName;
    private int populationCount;
    private String currentPresident;

    public String getOfficialName() {
        return officialName;
    }

    public void setOfficialName(String officialName) {
        this.officialName = officialName;
    }

    public int getPopulationCount() {
        return populationCount;
    }

    public void setPopulationCount(int populationCount) {
        this.populationCount = populationCount;
    }

    public String getCurrentPresident() {
        return currentPresident;
    }

    public void setCurrentPresident(String currentPresident) {
        this.currentPresident = currentPresident;
    }

}
