package com.buutcamp.countries;

public interface Country {

    String getOfficialName();

    void setOfficialName(String officialName);

    int getPopulationCount();

    void setPopulationCount(int populationCount);

    String getCurrentPresident();

    void setCurrentPresident(String currentPresident);

}
