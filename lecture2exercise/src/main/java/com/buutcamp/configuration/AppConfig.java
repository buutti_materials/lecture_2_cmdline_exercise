package com.buutcamp.configuration;

import com.buutcamp.countries.Country;
import com.buutcamp.countries.Finland;
import com.buutcamp.countries.Germany;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class AppConfig {

    @Bean("finland")
    @Lazy
    public Country getFinland() {
        Country country = new Finland();

        country.setCurrentPresident("Sauli Niinistö");
        country.setOfficialName("Republic of Finland");
        country.setPopulationCount(5517887);

        return country;
    }

    @Bean("germany")
    @Lazy
    public Country getGermany() {
        Country country = new Germany();

        country.setCurrentPresident("Frank-Walter Steinmeier");
        country.setOfficialName("Federal Republic of Germany");
        country.setPopulationCount(82800000);

        return country;
    }

    @Bean("china")
    @Lazy
    public Country getChina() {
        Country country = new Germany();

        country.setCurrentPresident("Xi Jinping");
        country.setOfficialName("People's Republic of China");
        country.setPopulationCount(1403500365);

        return country;
    }
}
