package com.buutcamp.main;

import com.buutcamp.configuration.AppConfig;
import com.buutcamp.countries.Country;
import com.buutcamp.inputhandle.InputHandle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunApp {

    public RunApp() {
        //Load the application context
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        //Custom made class to handle user inputs via methods
        InputHandle inputHandle = new InputHandle();

        //Load memory spot for country interface.
        Country country;
        //Start the main loop
        while (true) {
            /**
             * Main (while) loop of the program begins here
             */
            // Get user input and remove possible white spaces from the beginning with trim()
            // Also uses replaceAll \\s+ to replace long white spaces with 1 white space
            String userInput = inputHandle.getUserInput().trim().replaceAll("\\s+", " ");

            String method = "";
            if (userInput.length() > 2) {
                //Get method out of the string (First 3 letters)
                method = userInput.substring(0, 3).toLowerCase();
            }
            String inputCountry = "";
            if (userInput.length() > 4) {
                //Get country out of the string (From fourth index onwards)
                inputCountry = userInput.substring(4).toLowerCase();
            }

            //Act accordingly
            if (userInput.equals("exit")) {
                //Close ctx and return to MainClass and end the program
                ((AnnotationConfigApplicationContext) ctx).close();
                return;

            }//Check if the inputCountry is a valid country
            else if ( inputHandle.isValidCountry(inputCountry) ) {
                //If method is GET
                if (method.equals("get")) {
                    //Get the input country
                    country = ctx.getBean(inputCountry, Country.class);
                    System.out.println("Official name of the country: " + country.getOfficialName());
                    System.out.println("President: " + country.getCurrentPresident());
                    System.out.println("Population count: " + country.getPopulationCount());
                } //If method is SET
                else if (method.equals("set")) {
                    //Almost the same as get
                    //Only need to add one more condition
                    //Checking whether the user wants to change
                    //President, OfficialName or PopulationCount
                }
            } else {
                System.out.println("\n" + userInput + " is not a valid command (type exit to stop the program)");
            }

            /**
             * Main loop of the program ends here and jumps
             * back to beginning of the while loop
             *
             * Only get methods for the countries is implemented here
             * but implementing set methods via the command line should
             * be fairly trivial as it follows the same procedure
             */
        }

    }

}
