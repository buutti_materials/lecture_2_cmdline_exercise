package com.buutcamp.inputhandle;

import com.buutcamp.configuration.AppConfig;
import com.buutcamp.countries.Country;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class InputHandle {

    public String getUserInput() {
        System.out.println("\nUser input please: ");
        String userInput = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            userInput = bufferedReader.readLine();
        } catch (IOException e) {
            //If an error occurs, exit the program
            e.printStackTrace();
            userInput = "exit";
        }

        return userInput;
    }

    public boolean isValidCountry(String str) {
        // Get the app context
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        // Check if ctx.getBeanNamesForType() contains the user inputted
        // country which is in variable str
        if (Arrays.asList(ctx.getBeanNamesForType(Country.class)).contains(str)) {
            ((AnnotationConfigApplicationContext) ctx).close();
            return true;
        } else {
            ((AnnotationConfigApplicationContext) ctx).close();
            return false;
        }
    }

}
